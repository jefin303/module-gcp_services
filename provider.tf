
provider "google" {
      #path for GCP service account credentials
   credentials = "${file("keyfile.json")}"
      # GCP project ID
   project     = "augmented-clock-353309"
      # Any region of your choice
   region      = "us-central1"
      # Any zone of your choice      
   zone        = "us-central1-a"
}
